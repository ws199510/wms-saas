package com.wms.mapper.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wms.common.entity.Authority;

import java.util.List;

/**
 * <p>
 * 权限 Mapper 接口
 * </p>
 *
 * @author wangshen
 * @since 2020-06-11
 */
public interface AuthorityMapper extends BaseMapper<Authority> {

    List<Authority> test();
}

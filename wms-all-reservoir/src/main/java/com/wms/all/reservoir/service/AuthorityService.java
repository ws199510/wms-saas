package com.wms.all.reservoir.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wms.common.entity.Authority;

import java.util.List;

/**
 * <p>
 * 权限 服务类
 * </p>
 *
 * @author wangshen
 * @since 2020-06-11
 */
public interface AuthorityService extends IService<Authority> {

    List<Authority> test();
}

package com.wms.all.reservoir.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wms.mapper.mapper.AuthorityMapper;
import com.wms.all.reservoir.service.AuthorityService;
import com.wms.common.entity.Authority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 权限 服务实现类
 * </p>
 *
 * @author wangshen
 * @since 2020-06-11
 */
@Service
public class AuthorityServiceImpl extends ServiceImpl<AuthorityMapper, Authority> implements AuthorityService {

    @Autowired
    private AuthorityMapper authorityMapper;

    @Override
    public List<Authority> test() {
        return authorityMapper.test();
    }
}

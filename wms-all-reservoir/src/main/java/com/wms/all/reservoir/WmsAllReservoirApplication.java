package com.wms.all.reservoir;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Sink;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@EnableBinding(Sink.class)
@MapperScan(basePackages = {"com.wms.mapper.mapper"})
public class WmsAllReservoirApplication {

    public static void main(String[] args) {
        SpringApplication.run(WmsAllReservoirApplication.class, args);
    }

}

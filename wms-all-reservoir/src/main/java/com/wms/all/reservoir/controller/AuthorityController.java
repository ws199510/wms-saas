package com.wms.all.reservoir.controller;


import com.wms.all.reservoir.handler.ApiContext;
import com.wms.all.reservoir.service.AuthorityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 权限 前端控制器
 * </p>
 *
 * @author wangshen
 * @since 2020-06-11
 */
@RestController
@RequestMapping("/authority")
public class AuthorityController {

    @Autowired
    private AuthorityService authorityService;

    @Autowired
    private ApiContext apiContext;

    @RequestMapping("/test/{reservoirId}")
    public String test (@PathVariable("reservoirId") Long reservoirId) {
        apiContext.setCurrentProviderId(reservoirId);
        return authorityService.list().toString();
    }
    @RequestMapping("/test1/{reservoirId}")
    public String test1 (@PathVariable("reservoirId") Long reservoirId) {
        apiContext.setCurrentProviderId(reservoirId);
        return authorityService.test().toString();
    }

}

package com.wms.api.exception;

import com.wms.common.dto.BaseResDto;
import com.wms.common.enums.ResEnums;
import com.wms.common.exception.BaseException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 全局异常捕获
 */
@ControllerAdvice
@ResponseBody
@Slf4j
public class ExceptionHandlerAdvice {


    @ExceptionHandler(value = BaseException.class)
    public BaseResDto baseExceptionHandler (BaseException e) {
        BaseResDto baseResDto = new BaseResDto();
        baseResDto.setError(e);
        return baseResDto;
    }

    @ExceptionHandler(value = Exception.class)
    public BaseResDto exceptionHandler (Exception e) {
        log.error(e.getMessage());
        return new BaseResDto(ResEnums.SYS_ERROR);
    }
}

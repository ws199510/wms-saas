package com.wms.common.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 权限
 * </p>
 *
 * @author wangshen
 * @since 2020-06-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("wms_authority")
public class Authority extends Model<Authority> {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Long companyId;

    private Boolean deleted;

    private Date createDate;

    private Date modifyDate;

    private Date deleteDate;

    private Long reservoirId;

    /**
     * 编码
     */
    private String code;

    /**
     * 名称
     */
    private String name;

    /**
     * 描述
     */
    private String memo;

    /**
     * 菜单id
     */
    private Long menuId;

    /**
     * 路径
     */
    private String path;

    /**
     * 状态
     */
    private Boolean status;

    /**
     * 子系统
     */
    private Long subsystemId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}

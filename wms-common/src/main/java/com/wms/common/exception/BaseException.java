package com.wms.common.exception;

import com.wms.common.enums.ResEnums;

public class BaseException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private String code;
    private String errorMessage;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public BaseException (ResEnums resEnums, Object... arguments) {
        this.code = resEnums.getCode();
        this.errorMessage = String.format(resEnums.getMsg(), arguments);
    }
}

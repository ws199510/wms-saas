package com.wms.common.dto;


import com.wms.common.enums.ResEnums;
import com.wms.common.exception.BaseException;

import java.io.Serializable;

/**
 * ajax返回的统一信息类
 * 
 * @author ht
 * @time 2018.6.11
 */
public class BaseResDto<T> implements Serializable {

	private static final long serialVersionUID = 1L;

	private String code = ResEnums.SUCCESS.getCode();

	private T data;

	private String content;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * 错误设置
	 * 
	 * @param e
	 */
	public void setError(BaseException e) {
		this.code = e.getCode();
		this.content = e.getErrorMessage();
	}

	/**
	 * 错误设置
	 */
	public void setEnums(ResEnums enums) {
		this.code = enums.getCode();
		this.content = enums.getMsg();
	}

	public BaseResDto (ResEnums enums) {
	    this.code = enums.getCode();
	    this.content = enums.getMsg();
    }
    public BaseResDto () {}

    public static <T> BaseResDto<T> success(T e) {
		BaseResDto<T> baseResDto = new BaseResDto();
		baseResDto.setData(e);
		return baseResDto;
	}

    public static <T> BaseResDto<T> success(T e, String content) {
		BaseResDto<T> baseResDto = new BaseResDto();
		baseResDto.setData(e);
		baseResDto.setContent(content);
		return baseResDto;
	}

	public static BaseResDto error (BaseException e) {
		BaseResDto baseResDto = new BaseResDto();
		baseResDto.setError(e);
		return baseResDto;
	}

}

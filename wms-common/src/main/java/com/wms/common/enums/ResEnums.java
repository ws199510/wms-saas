package com.wms.common.enums;

/**
 * http请求返回的信息
 */
public enum ResEnums {
	//1、基础信息
	//2、登入信息
	//3、用户信息
	SUCCESS("100000", "成功"),
	SYS_ERROR("100001", "系统异常"),
	SERVER_LOST("100002","服务连接失败"),//微服务挂了
	LOST_FIELD("100003", "缺少字段%s");



	private String code;
	private String msg;

	private ResEnums(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}

	public String getMsg() {
		return msg;
	}

}

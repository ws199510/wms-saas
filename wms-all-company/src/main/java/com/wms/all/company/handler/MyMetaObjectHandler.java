package com.wms.all.company.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.logging.Logger;

/**
 * 自动填充
 *
 * @author: shuaishuai
 * @create: 2020-05-10-16:47
 */
@Component
@Slf4j
public class MyMetaObjectHandler implements MetaObjectHandler {


    /**
     * 创建时间
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        log.info(" -------------------- start insert fill ...  --------------------");
//        if (metaObject.hasGetter("gmtCreate") && metaObject.hasGetter("gmtModified")) {
//            setFieldValByName("gmtCreate", new Date(), metaObject);
//            setFieldValByName("gmtModified", new Date(), metaObject);
//        }
        if (metaObject.hasGetter("deleted")) {
            setFieldValByName("deleted", false, metaObject);
        }
        if (metaObject.hasGetter("createDate")) {
            setFieldValByName("createDate", new Date(), metaObject);
        }
    }

    /**
     * 最后一次更新时间
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        log.info(" -------------------- start update fill ...  --------------------");
        if (metaObject.hasGetter("modifyDate")) {
            setFieldValByName("modifyDate", new Date(), metaObject);
        }
    }
}
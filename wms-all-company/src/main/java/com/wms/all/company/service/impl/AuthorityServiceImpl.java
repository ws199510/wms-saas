package com.wms.all.company.service.impl;

import com.wms.common.entity.Authority;
import com.wms.mapper.mapper.AuthorityMapper;
import com.wms.all.company.service.AuthorityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 权限 服务实现类
 * </p>
 *
 * @author wangshen
 * @since 2020-06-11
 */
@Service
public class AuthorityServiceImpl extends ServiceImpl<AuthorityMapper, Authority> implements AuthorityService {

    @Autowired
    private AuthorityMapper authorityMapper;

    @Override
    public List<Authority> test() {
        return authorityMapper.test();
    }
}

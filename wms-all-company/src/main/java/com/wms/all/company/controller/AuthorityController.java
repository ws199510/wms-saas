package com.wms.all.company.controller;


import com.wms.all.company.handler.ApiContext;
import com.wms.all.company.service.AuthorityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 权限 前端控制器
 * </p>
 *
 * @author wangshen
 * @since 2020-06-11
 */
@RestController
@RequestMapping("/authority")
public class AuthorityController {

    @Autowired
    private AuthorityService authorityService;

    @Autowired
    private ApiContext apiContext;

    @RequestMapping("/test/{companyId}")
    public String test (@PathVariable("companyId") Long companyId) {
        apiContext.setCurrentProviderId(companyId);
        return authorityService.list().toString();
    }
    @RequestMapping("/test1/{companyId}")
    public String test1 (@PathVariable("companyId") Long companyId) {
        apiContext.setCurrentProviderId(companyId);
        return authorityService.test().toString();
    }

}
